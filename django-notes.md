# Django

# Terms
**Project**
* The place we put our main configuration

**App**
* A part of the overall project that contains code, HTML, and CSS

# Showing your own HTML in Django

These steps can be done in any order:

* Create the HTML file
  - Must be put in a subdirectory of the app named <em>templates</em>
* Create a <em>view</em> to use the HTML file
  - When you make an HTTP request to Django, it will look path the path in the URL to figure out what view to use
  - A view is just a Python function
* Associate the <em>view</em> to a URL path
* Let the project (<em>hello_world</em>) know about the app (<em>greetings</em>)
* Have the project use the URL paths defined in <em>greetings</em>

# How to Get Data Into Our Template

```py
from django.shortcuts import render

from greetings.models import Greeting

# Create your views here.
def show_greeting(request):
  greetings = Greeting.objects.all()
  context = {
    "greetings": greetings,
  }
  return render(request, "home.html", context)
  ```

1. Collect the data we want to show in our HTML:    `greetings = Greeting.objects.all()`
  * Whatever our model class, we write that name, then objects.all() to get everything from the database
  * The result is a list of Greeting objects, each with a **language** and a **phrase**

2. Create a dictionary of the data:   `context = { "greetings": greetings, }` 
  * Creates a dictionary with one entry in it. 
  * The **key** for the entry is the string **"greetings"**
    * Whatever the name of the **key** in the context dictionary becomes a variable to use in the Django template
  * The **value** for the entry is the list of greetings stored in the **greetings** variable

3. Pass that dictionary to Django to render the HTML file:    `return render(request, "home.html", context)`
  * Django's render function has an optional third parameter
    * The third parameter is the context for the template to use

# Purpose of Different Files
## urls.py
  * Creates a list of paths and stores it in a variable <em>urlpatterns</em>
  * Django loooks for that specific variable name as a list to use your URL paths
  
## models.py
  * Models allow you to work with a database
  * They help create a structure of data