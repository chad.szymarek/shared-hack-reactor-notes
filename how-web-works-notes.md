# The Three Tiers of a Full-Stack Application

A traditional full-stack application is made of three tiers:

1. Front-end

- Also refered to as the client, GUI (graphical user interface), GHI (graphical human interface)
- The Web browser sends requests to the back-end and shows the html that the back-end makes

2. Back-end

- Also reffered to as server, web server, and Django server
- The Web server answers requests from the front-end by using data from the database

3. Database

- The data for the Web application gets stored here for safekeeping and easy retrieval

# How they all work together

All three tiers interact with one another. Front-end talks to back-end, back-end talks to the database.
That full cycle is called the HTTP request-response cycle.

## The HTTP request-response cycle

### Request

1. The request starts when you type a URL into your browswer, like hackreactor.com
2. The back-end gets the request and figures out what data it needs from the database
3. The database gets the query from the back-end and search for that data in its storage

### Response

4. After finding the data, the database sends all of it back to the back-end
5. The back-end takes the data and merges it with a template to make html
6. The browser receives the HTML and displays it in the window for you

# Types of HTTP requests

**Get request**

- Asks for some HTML from the server.

**Post request**

- Sends data, usually a form, to the server and gets back a response.

# A Web Framework (Django in this case)

## What does a Web Framework do?

- Listen for and accept all the HTTP requests from people
- Figure out what part of your Python code shoudl generate a response
- Invoke your Python
- Send the response from your Python code back to the client
